package org.esprit.sport;

public class Formule {
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomF() {
		return NomF;
	}
	public void setNomF(String nomF) {
		NomF = nomF;
	}
	public int getNote() {
		return note;
	}
	public void setNote(int note) {
		this.note = note;
	}
	private int id;
	private String NomF;
	private int note;
	public Formule(int id, String nomF, int note) {
		super();
		this.id = id;
		NomF = nomF;
		this.note = note;
	}
	
	public Formule() {}
	
}